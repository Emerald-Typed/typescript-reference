"use client"
import Link from "next/link"
import { CodeBlock, atomOneLight } from "react-code-blocks"
import { useEffect, useState } from "react"
import Loading from "../components/loading"

type Utility = {
  Title: string
  useCase: string
  Typescript: string
}

const UtilityTypesPage = () => {
  const [Utilities, setData] = useState<Utility[]>([])

  let apiUrl = "/api?params=Utilities" // Default API endpoint for local development

  // Check if the environment is production
  if (process.env.NODE_ENV === "production") {
    apiUrl = "https://typescript-reference.vercel.app/api?params=Utilities"
  }
  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(apiUrl)
      const data = await response.json()
      console.log(data)
      setData(data)
    }
    fetchData()
  }, [])
  return (
    <div className="mx-auto lg:w-[60%] w-[100%] my-auto">
      <h1 className="loud-text">Utility Types:</h1>
      <ul className="flex flex-col font-medium gap-8 text-xl">
        {Utilities.length === 0 ? (
          <Loading />
        ) : (
          Utilities.map((element: Utility, index: number) => (
            <li key={index}>
              <div className="my-4 border-b border-emerald-400">
                <h1 className="text-4xl mb-4">
                  The{" "}
                  <span className="text-emerald-500/75">{element.Title}</span>{" "}
                  Utility Type
                </h1>
                <h2 className="text-2xl font-semibold mb-2">
                  {element.Title} UseCase:
                </h2>
                <pre className="bg-white text-black mb-2 p-4 text-base rounded-md overflow-x-auto">
                  {element.useCase}
                </pre>
                <h2 className="text-2xl font-semibold mb-2">
                  {element.Title} Code:
                </h2>
                <pre className="bg-white text-sm text-black p-4 mb-2 rounded-md overflow-x-auto">
                  <CodeBlock
                    text={element.Typescript}
                    language={"typescript"}
                    showLineNumbers={true}
                    theme={atomOneLight}
                  />
                </pre>
              </div>
            </li>
          ))
        )}
      </ul>
      <span className="flex justify-center mt-3">
        <button>
          <Link
            className="purpleButtons text-xl font-bold"
            href="https://www.typescriptlang.org/docs/handbook/utility-types.html"
            target="_blank"
          >
            <span className="sm:inline hidden"> ⮏ </span> Utitlity Types
            Documentation
          </Link>
        </button>
      </span>
    </div>
  )
}

export default UtilityTypesPage
