import { type NextRequest } from "next/server"

const Utilties = [
  {
    Title: "Partial",
    useCase:
      "Constructs a type with all properties of `Person` set to optional.",
    Typescript: `interface Person {
  name: string;
  age: number;
}

type PartialPerson = Partial<Person>;
// Result: { name?: string; age?: number; }`,
  },
  {
    Title: "Required",
    useCase:
      "Constructs a type with all properties of `Person` set to required.",
    Typescript: `interface Person {
  name?: string;
  age?: number;
}

type RequiredPerson = Required<Person>;
// Result: { name: string; age: number; }`,
  },
  {
    Title: "Readonly",
    useCase:
      "Constructs a type with all properties of `Person` set to readonly.",
    Typescript: `interface Person {
  name: string;
  age: number;
}

type ReadonlyPerson = Readonly<Person>;
// Result: { readonly name: string; readonly age: number; }`,
  },
  {
    Title: "Record",
    useCase:
      "Constructs a type with a set of properties `Fruit` of type `number`.",
    Typescript: `type Fruit = 'apple' | 'banana' | 'orange';
type FruitPrices = Record<Fruit, number>;
// Result: { apple: number; banana: number; orange: number; }`,
  },
  {
    Title: "Pick",
    useCase:
      "Constructs a type by picking the set of properties 'name' and 'age' from 'Person'.",
    Typescript: `interface Person {
  name: string;
  age: number;
  address: string;
}

type PersonInfo = Pick<Person, 'name' | 'age'>;
// Result: { name: string; age: number; }`,
  },
  {
    Title: "Omit",
    useCase:
      "Constructs a type by omitting the set of properties 'address' from 'Person'.",
    Typescript: `interface Person {
  name: string;
  age: number;
  address: string;
}

type PersonWithoutAddress = Omit<Person, 'address'>;
// Result: { name: string; age: number; }`,
  },
  {
    Title: "Exclude",
    useCase:
      "Exclude from 'Numbers' those types that are assignable to 2 or 4.",
    Typescript: `type Numbers = 1 | 2 | 3 | 4 | 5;
type OddNumbers = Exclude<Numbers, 2 | 4>;
// Result: 1 | 3 | 5`,
  },
  {
    Title: "ReturnType",
    useCase: "Extracts the return type of a function type.",
    Typescript: `type MyFunction = () => string;
type MyFunctionReturnType = ReturnType<MyFunction>;
// Result: string`,
  },
]

const Syntax = [
  {
    Title: "Basic Variable Types",
    useCase:
      "Examples of basic variable types in TypeScript(annotated & inferred).",
    Typescript: `// Type Annotation (Variable Declaration):
let age: number = 25
let name: string = "John"
let isStudent: boolean = true

// Type Inference:
let inferredAge = 25 // TypeScript infers 'number'
let inferredName = "John" // TypeScript infers 'string'
let inferredIsStudent = true // TypeScript infers 'boolean'
    `,
  },
  {
    Title: "Basic Function Types",
    useCase: "Examples of basic function types in TypeScript.",
    Typescript: `// Function Parameter Types:
function add(x: number, y: number): number {
return x + y;
}

// Optional Parameters:
function greet(name: string, greeting?: string): string {
return greeting ? \`\${greeting}, \${name}!\` : \`Hello, \${name}!\`;
}

// Default Parameters:
function multiply(x: number, y: number = 2): number {
return x * y;
}`,
  },
  {
    Title: "Object Types",
    useCase: "Examples of object types in TypeScript.",
    Typescript: `// Object Types:
let person: { name: string; age: number } = {
name: "Alice",
age: 30,
};`,
  },
  {
    Title: "Tuples",
    useCase: "Examples of tuples in TypeScript.",
    Typescript: `// Tuple:
let person: [string, number] = ["Alice", 30];`,
  },

  {
    Title: "Union and Intersection Types",
    useCase: "Examples of union and intersection types in TypeScript.",
    Typescript: `// Union Types:
let id: string | number = "abc123";

// Intersection Types:
interface Car {
brand: string;
model: string;
}

interface ElectricCar {
batteryType: string;
}

let electricCar: Car & ElectricCar = {
brand: "Tesla",
model: "Model S",
batteryType: "Lithium-ion",
};`,
  },
  {
    Title: "Type Assertion and Aliases",
    useCase: "Examples of type assertion and aliases in TypeScript.",
    Typescript: `// Type Assertion (Type Casting):
let userInput: any = "Hello World!";
let strLength: number = (<string>userInput).length;

// Type Aliases:
type Point = {
x: number;
y: number;
};`,
  },
  {
    Title: "Enums",
    useCase: "Examples of enums in TypeScript.",
    Typescript: `// Enums:
enum Color {
Red,
Green,
Blue,
}

let myColor: Color = Color.Green;`,
  },
]
const Methods = [
  {
    Title: "split(separator)",
    useCase:
      "Splits a string into an array of substrings based on a specified separator.",
    Typescript: `const sentence = "This is a sample sentence";
const words = sentence.split(" ");
// Result: ["This", "is", "a", "sample", "sentence"]`,
  },
  {
    Title: "join(separator)",
    useCase:
      "Joins all elements of an array into a string, using a specified separator.",
    Typescript: `const words = ["This", "is", "a", "sample", "sentence"];
const sentence = words.join(" ");
// Result: "This is a sample sentence"`,
  },
  {
    Title: "pop()",
    useCase: "Removes the last element from an array and returns that element.",
    Typescript: `const fruits = ["apple", "banana", "orange"];
const lastFruit = fruits.pop();
// Result: lastFruit = "orange", fruits = ["apple", "banana"]`,
  },
  {
    Title: "push(element1, ..., elementN)",
    useCase: "Adds one or more elements to the end of an array.",
    Typescript: `const fruits = ["apple", "banana"];
fruits.push("orange", "grape");
// Result: fruits = ["apple", "banana", "orange", "grape"]`,
  },
  {
    Title: "shift()",
    useCase:
      "Removes the first element from an array and returns that element.",
    Typescript: `const fruits = ["apple", "banana", "orange"];
const firstFruit = fruits.shift();
// Result: firstFruit = "apple", fruits = ["banana", "orange"]`,
  },
  {
    Title: "unshift(element1, ..., elementN)",
    useCase: "Adds one or more elements to the beginning of an array.",
    Typescript: `const fruits = ["banana", "orange"];
fruits.unshift("apple", "grape");
// Result: fruits = ["apple", "grape", "banana", "orange"]`,
  },
  {
    Title: "splice(start, deleteCount, item1, ..., itemN)",
    useCase:
      "Changes the contents of an array by removing or replacing existing elements and/or adding new elements in place.",
    Typescript: `const numbers = [1, 2, 3, 4, 5];
numbers.splice(2, 1, 6, 7);
// Result: numbers = [1, 2, 6, 7, 4, 5]`,
  },

  {
    Title: "filter()",
    useCase:
      "Creates a new array with all elements that pass the test implemented by the provided function.",
    Typescript: `const numbers = [1, 2, 3, 4, 5]; 
const evenNumbers = numbers.filter((num) => num % 2 === 0); 
// Result: evenNumbers = [2, 4]`,
  },

  {
    Title: "JSON.parse()",
    useCase:
      "Parses a JSON string, constructing the JavaScript value or object described by the string.",
    Typescript: `const jsonString = '{"name": "John", "age": 30, "city": "New York"}'; 
const personObject = JSON.parse(jsonString); 
// Result: personObject = { name: 'John', age: 30, city: 'New York' }`,
  },
  {
    Title: "parseInt(string, radix)",
    useCase: "Parses a string argument and returns an integer.",
    Typescript: `const numberString = "123";
const number = parseInt(numberString, 10);
// Result: number = 123`,
  },
]
export async function GET(request: NextRequest) {
  const searchParams = request.nextUrl.searchParams
  const query = searchParams.get("params")
  if (query === "Utilities") {
    return Response.json(Utilties)
    // /api?params=Utilties
  }
  if (query === "Methods") {
    return Response.json(Methods)
    // /api?params=Methods
  }
  if (query === "Syntax") {
    return Response.json(Syntax)
    // /api?params=Syntax
  }
  return new Response("Invalid query parameter", { status: 400 })
}
