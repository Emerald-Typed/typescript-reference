import Link from "next/link"
export default function Home() {
  return (
    <div className="flex flex-col mx-auto min-w-[60%] max-w-7xl my-auto text-center">
      <h1 className="loud-text">Welcome To My TypeScript Reference Page!!!!</h1>
      <h1 className="text-xl font-extrabold">-Kyle Hipple</h1>
      <ul className="flex text-center mt-8 text-2xl text-xl sm:flex-row flex-col">
        <Link className="purpleButtons " href="/Utility-Types">
          Utility Types
        </Link>
        <Link className="purpleButtons" href="/Type-Syntax">
          Type Syntax
        </Link>
        <Link className="purpleButtons" href="/Basic-Methods">
          Basic JavaScript Methods
        </Link>
      </ul>
    </div>
  )
}
