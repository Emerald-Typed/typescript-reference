import Link from "next/link"
export default function Footer() {
  return (
    <div className="bg-violet-400/50">
      <div className="bg-gradient-to-r from-emerald-400 via-violet-400 to-purple-400">
        <div className="w-full bg-emerald-400/75 text-center text-lg font-semibold leading-8 text-white min-h-full">
          <Link
            className="hover:text-purple-400/60 text-2xl "
            href={"https://gitlab.com/Emerald-Typed/typescript-reference"}
            target="_blank"
            rel="noopener noreferrer"
          >
            {" "}
            <span className="text-violet-400/60 font-extrabold">
              Repo Link
            </span>{" "}
            🦖
            <span className="text-violet-400/60 font-extrabold">
              {" "}
              for Project
            </span>
          </Link>
          <h2>Built with 💚</h2>
        </div>
      </div>
    </div>
  )
}
