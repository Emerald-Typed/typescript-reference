export default function Loading() {
  return (
    <div className="my-4 border-b border-emerald-400">
      <h1 className="text-4xl mb-4">
        <span className="text-emerald-500/75">Loading</span> ....
      </h1>
    </div>
  )
}
