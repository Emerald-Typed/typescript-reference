"use client"
import { CodeBlock, atomOneLight } from "react-code-blocks"
import { useState, useEffect } from "react"
import Loading from "../components/loading"

type Syntax = {
  Title: string
  useCase: string
  Typescript: string
}
const TypeSyntaxPage = () => {
  const [Syntax, setData] = useState<Syntax[]>([])
  let apiUrl = "/api?params=Syntax" // Default API endpoint for local development

  // Check if the environment is production
  if (process.env.NODE_ENV === "production") {
    apiUrl = "https://typescript-reference.vercel.app/api?params=Syntax"
  }
  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(apiUrl)
      const data = await response.json()
      console.log(data)
      setData(data)
    }
    fetchData()
  }, [])
  return (
    <div className="mx-auto lg:w-[60%] w-[100%] my-auto">
      <h1 className="loud-text">Type Syntax:</h1>
      <ul className="flex flex-col font-medium gap-8 text-xl">
      {Syntax.length === 0 ? (
          <Loading />
        ) : (
          Syntax.map((element: Syntax, index: number) => (
          <li key={index}>
            <div className="my-4 border-b border-emerald-400">
              <h1 className="text-4xl mb-4">
                <span className="text-emerald-500/75">{element.Title}</span>{" "}
                Syntax
              </h1>
              <h2 className="text-2xl font-semibold mb-2">
                {element.Title} UseCase:
              </h2>
              <pre className="bg-white text-black text-base mb-2 p-4 rounded-md overflow-x-auto">
                {element.useCase}
              </pre>
              <h2 className="text-2xl font-semibold mb-2">
                {element.Title} Code:
              </h2>
              <pre className="bg-white text-sm text-black p-4  mb-2 rounded-md overflow-x-auto">
                <CodeBlock
                  text={element.Typescript}
                  language={"typescript"}
                  showLineNumbers={true}
                  theme={atomOneLight}
                />
              </pre>
            </div>
          </li>
        ))
      )}
      </ul>
    </div>
  )
}

export default TypeSyntaxPage
