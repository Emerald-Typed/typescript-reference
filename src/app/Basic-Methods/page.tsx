"use client"
import { CodeBlock, atomOneLight } from "react-code-blocks"
import { useState, useEffect } from "react"
import Loading from "../components/loading"

type Method = {
  Title: string
  useCase: string
  Typescript: string
}

const BasicMethodsPage = () => {
  const [Methods, setData] = useState<Method[]>([])

  let apiUrl = "/api?params=Utilities" // Default API endpoint for local development

  // Check if the environment is production
  if (process.env.NODE_ENV === "production") {
    apiUrl = "https://typescript-reference.vercel.app/api?params=Utilities"
  }
  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch("/api?params=Methods")
      const data = await response.json()
      console.log(data)
      setData(data)
    }
    fetchData()
  }, [])
  return (
    <div className="mx-auto lg:w-[60%] w-[100%] my-auto">
      <h1 className="loud-text">Basic JavaScript Methods:</h1>
      <ul className="flex flex-col font-medium gap-8 text-xl">
        {Methods.length === 0 ? (
          <Loading />
        ) : (
          Methods.map((element: Method, index) => (
            <li key={index}>
              <div className="my-4 border-b border-emerald-400">
                <h1 className="text-4xl mb-4">
                  The{" "}
                  <span className="text-emerald-500/75">{element.Title}</span>{" "}
                  Method
                </h1>
                <h2 className="text-2xl font-semibold mb-2">
                  {element.Title} UseCase:
                </h2>
                <pre className="bg-white text-black text-base mb-2 p-4 rounded-md overflow-x-auto">
                  {element.useCase}
                </pre>
                <h2 className="text-2xl font-semibold mb-2">
                  {element.Title} Code:
                </h2>
                <pre className="bg-white text-black p-4 text-sm mb-2 rounded-md overflow-x-auto">
                  <CodeBlock
                    text={element.Typescript}
                    language={"typescript"}
                    showLineNumbers={true}
                    theme={atomOneLight}
                  />
                </pre>
              </div>
            </li>
          ))
        )}
      </ul>
    </div>
  )
}

export default BasicMethodsPage
