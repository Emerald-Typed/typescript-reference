import type { Metadata } from "next"
import { Inter } from "next/font/google"
import "./globals.css"
import Nav from "@/app/components/NavBar/nav"
import Footer from "@/app/components/NavBar/footer"

const inter = Inter({ subsets: ["latin"] })

export const metadata: Metadata = {
  title: "TypeScript Reference Page",
  description: "TypeScript Reference Page with all the good stuff!!!",
  icons: "/static/diamondE.png",
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <Nav />
        <div className="min-h-screen flex flex-col p-2 m-5">{children}</div>
        <Footer />
      </body>
    </html>
  )
}
