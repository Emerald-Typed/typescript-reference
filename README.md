<a name="readme-top"></a>
<br />

<div align="center">
<a href="https://typescript-reference.vercel.app/">
<img src="https://gitlab.com/Emerald-Typed/readme/-/raw/main/public/LOGO.png" alt="Logo">
</a>

<h3 align="center">Emerald Typed Reference Page</h3>

<p align="center">
A TypeScript Reference Page for Utility Types, Type/Interface Syntax and Basic Javascript Methods.
<br />
<a href="https://typescript-reference.vercel.app/"><strong>View Deployment  »</strong></a>
<br />
<a href="https://gitlab.com/Emerald-Typed/typescript-reference"><strong>Explore the Repo »</strong></a>
<br />
<a href="https://emerald-typed.vercel.app/contact">Contact Me</a>
·
<a href="https://gitlab.com/Emerald-Typed/typescript-reference/issues">Report Bug</a>
·
<a href="https://gitlab.com/Emerald-Typed/typescript-reference/issues">Request Feature</a>
</p>
</br>

[![Last Commit][commit-badge]](https://gitlab.com/Emerald-Typed/typescript-reference)

</div>

## Table of Contents

#### [About](#about-the-project)

#### [ScreenShot](#screenshot)

#### [Libraries](#built-with)

#### [Usage](#usage)

#### [Contact](#Contact)

#### [License](#license)

## About The Project

### ScreenShot

![Project Screen Shot][Screenshot]

Welcome to the Next.js TypeScript Reference app! The primary focus is on very concise and basic cases, providing straightforward examples to help you quickly grasp essential concepts. While things can get complex, these archetypes are designed to be go-to reference for fundamental TypeScript use cases.
<br>

The data is loaded dynamically with JSON data fed from next api routes.
<br>
Use a **GET** request query my api: https://typescript-reference.vercel.app/api?params=Utilities
<br>

Contains 3 sections that cover fundamental aspects that are invaluable for any TypeScript project. My goal was the most basic examples of what I would use when building robust and scalable applications. To aid in my the learning process, and work. It has helped me grow my understanding feel free to explore the provided examples to solidify your understanding as well.🚀

### Features

- **1. Utility Types:** Explore a collection of utility types that enhance the functionality and expressiveness of TypeScript. Learn how to leverage these types to improve your codebase and streamline development.

- **2. Type Syntax:** Master the art of TypeScript type syntax through clear and concise examples. Understand the fundamentals of defining types, interfaces, and leveraging advanced type features to enhance code clarity and maintainability.

- **3. Javascript Methods:** Discover essential JavaScript methods integrated seamlessly with Next.js. These methods are accessed through API routes, providing practical examples and straightforward explanations to assist you in incorporating them into your projects.

## Libraries

### Prerequisites

Required libraries and how to install them.

[![Node][Node.js]][Node-url]

[![NPM][NPM]][NPM-url]

### Built With

[![Next.js](https://img.shields.io/badge/next.js-34d399?logo=next.js&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge)](https://nextjs.org/)
[![React](https://img.shields.io/badge/react-34d399?logo=react&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge)](https://reactjs.org/)
[![Prisma](https://img.shields.io/badge/prisma-34d399?logo=prisma&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge)](https://www.prisma.io/)
[![PostgreSQL](https://img.shields.io/badge/postgresql-34d399?logo=postgresql&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge)](https://www.postgresql.org/)
[![TypeScript](https://img.shields.io/badge/typescript-34d399?logo=typescript&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge)](https://www.typescriptlang.org/)
[![Tailwind](https://img.shields.io/badge/tailwindcss-34d399?logo=tailwindcss&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge)](https://tailwindcss.com/)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Setup and Use

### Usage

1. **Visit the Deployed Page:**
   Open your browser and navigate to the [TypeScript Reference App](https://typescript-reference.vercel.app/).
   </br>

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Contact

Kyle Hipple - [Emerald-Typed Gitlab](https://gitlab.com/Emerald-Typed) - [Contact Me](https://emerald-typed.vercel.app/contact)

Project Link: [https://gitlab.com/Emerald-Typed/typescript-reference](https://gitlab.com/Emerald-Typed/typescript-reference)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## License

All Rights Reserved License. See [License.txt](https://gitlab.com/Emerald-Typed/typescript-reference/-/raw/main/LICENSE.txt)
for more information.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

[commit-badge]: https://img.shields.io/gitlab/last-commit/Emerald-Typed/typescript-reference?label=last%20commit&style=for-the-badge&color=34d399&labelColor=%23a1a1aa
[Screenshot]: https://gitlab.com/Emerald-Typed/typescript-reference/-/raw/main/public/static/screenshot.png
[NPM]: https://img.shields.io/badge/npm-34d399?logo=npm&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge
[NPM-url]: https://www.npmjs.com/
[Node.js]: https://img.shields.io/badge/node.js-34d399?logo=node.js&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge
[Node-url]: https://nodejs.org/en/
[React.js]: https://img.shields.io/badge/react-34d399?logo=react&logoColor=white&labelColor=%23a1a1aa&color=34d399&style=for-the-badge
[React-url]: https://reactjs.org/
